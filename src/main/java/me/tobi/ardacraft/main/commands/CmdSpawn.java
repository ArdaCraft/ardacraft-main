package me.tobi.ardacraft.main.commands;

import me.tobi.ardacraft.api.classes.Charakter;
import me.tobi.ardacraft.api.classes.Utils.Attitude;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CmdSpawn implements CommandExecutor{

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if(sender instanceof Player) {
			Player p = (Player)sender;
			if(args.length == 1) {
				p = Bukkit.getPlayer(args[1]);;
			}
			Charakter c = Charakter.get(p);
			if(c != null) {
				if(c.getRasse().getAttitude() == Attitude.GOOD) {
					p.teleport(new Location(Bukkit.getWorld("RPG"), 5534, 69, -3525));
				}else {
					p.teleport(new Location(Bukkit.getWorld("RPG"), 1842, 91, 2524));
				}
				p.sendMessage("§aDu wurdest zum Spawn teleportiert.");
			}else {
				p.teleport(Bukkit.getWorld("world").getSpawnLocation());
				p.sendMessage("§aDu wurdest zum Spawn teleportiert.");
			}
		}else {
			System.out.println("Dieser Befehl ist nur für Spieler.");
		}
		return true;
	}
	
	

}
