package me.tobi.ardacraft.main.commands;

import me.tobi.ardacraft.api.ArdaCraftAPI;
import me.tobi.ardacraft.api.classes.Charakter;
import me.tobi.ardacraft.api.classes.Rank;
import me.tobi.ardacraft.api.classes.Utils;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import ru.tehkode.permissions.PermissionUser;
import ru.tehkode.permissions.bukkit.PermissionsEx;

import java.util.Date;

public class CmdPlayer implements CommandExecutor{

	Player p = null;
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if(sender instanceof Player) {
			p = (Player)sender;
			if(args.length > 0) {
				Player target = Bukkit.getPlayer(args[0]);
				if(p.isOnline()) {
					if(args.length > 1) {
						if(args[1].equalsIgnoreCase("get")) {
							if(args.length > 2) {
								if(args[2].equalsIgnoreCase("health") || args[2].equalsIgnoreCase("leben") || args[2].equalsIgnoreCase("gesundheit")) {
									pr("Leben", target.getHealth());
								}else if(args[2].equalsIgnoreCase("hunger") || args[2].equalsIgnoreCase("hungrigkeit") || args[2].equalsIgnoreCase("sättigung")) {
									pr("Hunger", target.getSaturation());
								}else if(args[2].equalsIgnoreCase("xplevel") || args[2].equalsIgnoreCase("erfahrungslevel") || args[2].equalsIgnoreCase("level") || args[2].equalsIgnoreCase("xp") || args[2].equalsIgnoreCase("erfahrung") || args[2].equalsIgnoreCase("experience") || args[2].equalsIgnoreCase("xplevel")) {
									double xp = target.getExp();
									int lvl = target.getExpToLevel();
									double xp_ = Double.valueOf(String.valueOf(xp).substring(0, 4));
									pr("Erfahrungs Level", xp_ + lvl);
								}else if(args[2].equalsIgnoreCase("rasse") || args[2].equalsIgnoreCase("group") || args[2].equalsIgnoreCase("prefix") || args[2].equalsIgnoreCase("volk")) {
									pr("Volk", PermissionsEx.getUser(target).getPrefix());
								}else if(args[2].equalsIgnoreCase("rank") || args[2].equalsIgnoreCase("rang")) {
									pr("Rang", Rank.getPrefix(target));
								}else if(args[2].equalsIgnoreCase("location") || args[2].equalsIgnoreCase("loc") || args[2].equalsIgnoreCase("pos") || args[2].equalsIgnoreCase("position")) {
									pr("Position", "X: " + target.getLocation().getBlockX() + ", Y: " + target.getLocation().getBlockY() + ", Z: " + target.getLocation().getBlockZ());
								}else if(args[2].equalsIgnoreCase("character") || args[2].equalsIgnoreCase("charakter") || args[2].equalsIgnoreCase("char") || args[2].equalsIgnoreCase("cha")) {
									pr("Charakter: ", "[" + Charakter.get(target).getRasse() + "] " + Charakter.get(target).getName() + ", seit " + new Date(ArdaCraftAPI.getACDatabase().getUserManager().get(target.getUniqueId().toString()).getDateCharacterChanged()).toString());
								}else if (args[2].equalsIgnoreCase("firstjoin")) {
									pr("First Join:", new Date(target.getFirstPlayed()).toString());
								}else{
									help();
								}
							}else {
								help();
							}
						}else if(args[1].equalsIgnoreCase("set")) {
							if(args.length > 3) {
								if(args[2].equalsIgnoreCase("rasse") || args[2].equalsIgnoreCase("group") || args[2].equalsIgnoreCase("prefix") || args[2].equalsIgnoreCase("volk")) {
									if(Rank.get(p).isHigherThen(Rank.SPIELER)) {
										PermissionUser user = PermissionsEx.getUser(target);
										user.setPrefix(Utils.normalize(args[3]), null);
										p.sendMessage("§6" + target.getName() + "'s §aVolk wurde auf " + args[3] + " gesetzt!");
									}else {
										p.sendMessage("§cDu hast nicht genügend Rechte für diesen Befehl!");
									}
								}else if(args[2].equalsIgnoreCase("rank") || args[2].equalsIgnoreCase("rang")) {
									if(Rank.get(p).isHigherThen(Rank.MOD)) {
										PermissionUser user = PermissionsEx.getUser(target);
										user.setSuffix(args[3].toLowerCase(), null);
										p.sendMessage("§6" + target.getName() + "'s §aRank wurde auf " + args[3] + " gesetzt!");
									}else {
										p.sendMessage("§cDu hast nicht genügend Rechte für diesen Befehl!");
									}
								}else if(args[2].equalsIgnoreCase("nick") || args[2].equalsIgnoreCase("nickname") || args[2].equalsIgnoreCase("prefix") || args[2].equalsIgnoreCase("charakter")) {
									if(Rank.get(p).isHigherThen(Rank.SPIELER)) {
										target.setDisplayName(args[3]);
										p.sendMessage("§6" + target.getName() + "'s §aCharakter-Name wurde auf " + target.getDisplayName() + " gesetzt!");
									}else {
										p.sendMessage("§cDu hast nicht genügend Rechte für diesen Befehl!");
									}
								}else{
									help();
								}
							}
						}else {
							help();
						}
					}else {
						help();
					}
				}else {
					p.sendMessage("§cBitte gib einen Spieler an, der online ist!");
				}
			}else {
				help();
			}
		}
		return true;
	}
	
	public void help() {
		p.sendMessage("§2Hilfe:");
		Rank pr = Rank.get(p);
		if(pr == Rank.SPIELER) {
			
		}else if(pr == Rank.CMOD) {
			
		}else if(pr == Rank.MOD) {
			
		}else if(pr == Rank.ADMIN) {
			
		}else {
			p.sendMessage("§cBeim Anzeigen der Hilfe ist ein Fehler aufgetreten: Unregistered Rank Exception");
		}
		p.sendMessage("... ist noch in Arbeit");
	}
	
	public void pr(String param, Object wert) {
		p.sendMessage("§6" + param + ": §b" + String.valueOf(wert));
	}

}
