package me.tobi.ardacraft.main.commands;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class CmdChest implements CommandExecutor{

	@SuppressWarnings("deprecation")
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if(!(sender instanceof Player)) {
			if(args.length == 2 || args.length == 3) {
				Player target = Bukkit.getPlayer(args[1]);
				if(target.isOnline()) {
					ItemStack item = null;
					if(isNumeric(args[0])) {
						item = new ItemStack(Integer.valueOf(args[0]));
						if(args.length == 3) {
							item = new ItemStack(Integer.valueOf(args[0]), 64, (short)0, Byte.valueOf(args[2]));
						}
					}else if(isMaterial(args[0])){
						item = new ItemStack(Material.valueOf(args[0].toUpperCase()));
					}else {
						target.sendMessage("§cDieses Item wurde nicht gefunden!");
						return true;
					}
					Inventory inv = Bukkit.createInventory(target, 3*9);
					item.setAmount(item.getMaxStackSize());
					for(int i = 0; i < 3*9; i++) {
						inv.addItem(item);
					}
					target.openInventory(inv);
				}
			}
		}else {
			sender.sendMessage("Dieser Befehl kann nur über ServerSigns/die Konsole ausgeführt werden!");
		}
		return true;
	}
	
	public static boolean isNumeric(String value) {
		try {
			int number = Integer.parseInt(value);
			return number > 0;
		} catch (NumberFormatException e) {
			return false;
		}
	}
	
	public static boolean isMaterial(String value) {
		try {
			@SuppressWarnings("unused")
			Material material = Material.valueOf(value.toUpperCase());
			return true;
		} catch (Exception e) {
			return false;
		}
	}

}
