package me.tobi.ardacraft.main.commands;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import me.tobi.ardacraft.api.classes.Charakter;
import me.tobi.ardacraft.api.classes.Rank;
import me.tobi.ardacraft.api.classes.Rasse;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CmdList implements CommandExecutor{

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if(sender instanceof Player) {
			Player p = (Player)sender;
			HashMap<Rasse, List<Player>> rassen = new HashMap<Rasse, List<Player>>();
			for(Player pl : Bukkit.getOnlinePlayers()) {
				Rasse r = Rasse.get(p);
				if(rassen.get(r) == null) {
					List<Player> players = new ArrayList<Player>();
					rassen.put(r, players);
				}
				rassen.get(r).add(pl);
			}
			p.sendMessage("§aEs sind " + Bukkit.getOnlinePlayers().size() + " Spieler online: ");
			String players = "";
			for(Rasse r: rassen.keySet()) {
				for(Player pl : rassen.get(r)) {
					String color = Rank.get(pl) != Rank.SPIELER?"§c":"§a";
					//xy
					if(Charakter.get(pl) != null) {
						players += color + Charakter.get(pl).getName() + "/" + pl.getName() + "§r, ";
					}else {
						players += "§r" + pl.getName() + "§r, ";						
					}
				}
			}
			players.substring(0, players.toCharArray().length - 4); //TODO substring is ignored
			p.sendMessage(players);
		}
		return true;
	}

	
	
}
