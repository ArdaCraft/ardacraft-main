package me.tobi.ardacraft.main.commands;

import me.tobi.ardacraft.api.classes.Rank;
import me.tobi.ardacraft.main.Statics;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CmdMute implements CommandExecutor{

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if(sender instanceof Player) {
			Player p = (Player)sender;
			if(args.length == 1) {
				if(Rank.get(p).isHigherThen(Rank.SPIELER)) {
					Player target = Bukkit.getPlayer(args[0]);
					if(target.isOnline()) {
						if(!Statics.muted.contains(target)) {
							Statics.muted.add(target);	
							p.sendMessage("§cDu wurdest von " + p.getName() + " gemuted!");
						}else {
							Statics.muted.remove(target);		
							p.sendMessage("§aDu wurdest wieder entmuted!");
						}					
					}else {
						p.sendMessage("§c" + target + " ist momentan nicht online!");
					}
				}
			}else {
				p.sendMessage("§c???");
			}
		}
		return true;
	}	
	
}
