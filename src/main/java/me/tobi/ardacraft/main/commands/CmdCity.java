package me.tobi.ardacraft.main.commands;

import me.tobi.ardacraft.api.ArdaCraftAPI;
import me.tobi.ardacraft.api.classes.City;
import me.tobi.ardacraft.api.classes.Rank;
import me.tobi.ardacraft.api.classes.Region;
import me.tobi.ardacraft.api.classes.User;
import me.tobi.ardacraft.api.message.FancyMessage;
import net.minecraft.server.v1_10_R1.IChatBaseComponent;
import net.minecraft.server.v1_10_R1.PacketPlayOutChat;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.v1_10_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;

import java.util.HashMap;

public class CmdCity implements CommandExecutor{

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if(sender instanceof Player) {
			Player p = (Player)sender;
			if(args.length == 0) {
				showHelp(p);
				return true;
			}
			if (args[0].equalsIgnoreCase("create")) {
				if(Rank.get(p).isHigherThen(Rank.CMOD)) {
					if (args.length == 3) {

						boolean isGood;
						if (args[2].equalsIgnoreCase("gut") || args[2].equalsIgnoreCase("good")) {
							isGood = true;
						}else if(args[2].equalsIgnoreCase("boese") || args[2].equalsIgnoreCase("bad") || args[2].equalsIgnoreCase("böse")){
							isGood = false;
						}else{
							p.sendMessage("§cBitte gib eine gültige Gesinnung an! Mögliche Optionen sind gut oder böse");
							return true;
						}
						City.create(args[1], User.getByUUID(p.getUniqueId().toString()), p.getLocation(), isGood, new HashMap<>());
						//City c = new City(0, args[1], User.getByUUID(p.getUniqueId().toString()), p.getLocation(), isGood, null, 0);
						//ArdaCraftAPI.getACDatabase().getCityManager().add(c);
						p.sendMessage("§aStadt wurde gespeichert!");
					} else {
						p.sendMessage("§cUsage: /city create <Stadtname> <Owner> <Gesinnung>");
					}
				}else {
					p.sendMessage("§cDu hast keine Berechtigung für diesen Befehl.");
				}
			} else if (args[0].equalsIgnoreCase("delete")) {
				if(Rank.get(p).isHigherThen(Rank.CMOD)) {
					if (args.length == 2) {
						City c = ArdaCraftAPI.getACDatabase().getCityManager().get(args[1]);
						if (c == null) {
							p.sendMessage("§cBitte gib eine gültige Stadt an!");
							return true;
						}
						ArdaCraftAPI.getACDatabase().getCityManager().delete(c.getName());
						p.sendMessage("§aStadt wurde entfernt!");
					} else {
						p.sendMessage("§cUsage: /city delete <Stadtname>");
					}
				}else {
					p.sendMessage("§cDu hast keine Berechtigung für diesen Befehl.");
				}
			} else if(args[0].equalsIgnoreCase("tp")) {
				if(Rank.get(p).isHigherThen(Rank.SPIELER)) {
					if(args.length == 2) {
						City c = ArdaCraftAPI.getACDatabase().getCityManager().get(args[1]);
						if(c == null) {
							p.sendMessage("§cBitte gib eine gültige Stadt an!");
						}else {
							p.sendMessage("§aDu wurdest nach " + c.getName() + " teleportiert!");
							p.teleport(c.getLocation());
						}
					}else {
						p.sendMessage("§cBenutzung: /city tp <Stadtname>");
					}
				}else {
					p.sendMessage("§cDu hast keine Berechtigung für diesen Befehl.");
				}
			}else if (args[0].equalsIgnoreCase("list")) {
				p.sendMessage("§eEingetragene Städte:");
				p.sendMessage("=========================");
				for (City c : ArdaCraftAPI.getACDatabase().getCityManager().getAll()) {
					if (c.isGood()) {
						p.sendMessage("§a" + c.getName());
					} else if (!c.isGood()) {
						p.sendMessage("§7" + c.getName());
					}
				}
				p.sendMessage("=========================");
			} else if (args[0].equalsIgnoreCase("radar")) {
			    boolean noCityNear = true;
				for (City c : ArdaCraftAPI.getACDatabase().getCityManager().getAll()) {
					if(!c.getName().equalsIgnoreCase("Spawn")) {
						double distance = Math.sqrt((c.getLocation().getX() - p.getLocation().getBlockX())
								* (c.getLocation().getX() - p.getLocation().getBlockX())
								+ (c.getLocation().getBlockZ() - p.getLocation().getBlockZ())
								* (c.getLocation().getBlockZ() - p.getLocation().getBlockZ()));
						if(distance > 1000){
						    noCityNear = false;
                            break;
                        }
					}
				}

				if(noCityNear){
				    p.sendMessage("§aEs sind keine Städte näher als 1000 Blöcke von dir, hier darfst du eine Stadt gründen!");
                }else{
                    p.sendMessage("§cDu bist leider zu nah an einer oder mehreren Städten, um eine neue Stadt gründen zu können!");
                }

			} else if (args[0].equalsIgnoreCase("info")) {
				if(args.length == 2) {
					City c = ArdaCraftAPI.getACDatabase().getCityManager().get(args[1]);
					if(c != null) {
						p.sendMessage("§e=====================Stadtinfo=====================");
						p.sendMessage("§aName: " + c.getName());
						p.sendMessage("§aGründer: " + c.getOwner().getLastName());
						String attitude = c.isGood()?"§aGut":"§7Böse";
						p.sendMessage("§aGesinnung: " + attitude);
						p.sendMessage("§aEntfernung: " + (int)p.getLocation().distance(c.getLocation()) + " Blöcke");
						if(Rank.get(p) != Rank.SPIELER) {
							String base = "§aPosition: X:" + c.getLocation().getBlockX() + ", Z:" + c.getLocation().getBlockZ() + " ";
							String s = new FancyMessage(base).then("§r[§cTP§r]").command("/city tp " + c.getName()).tooltip("TP nach " + c.getName()).toJSONString();		

							IChatBaseComponent comp = IChatBaseComponent.ChatSerializer.a(s);
							PacketPlayOutChat packet = new PacketPlayOutChat(comp);
							((CraftPlayer) p).getHandle().playerConnection.sendPacket(packet);
						}
						p.sendMessage("§aRegionen:");
						for(Region r : ArdaCraftAPI.getACDatabase().getRegionManager().get(c)) {
							String state = "";
							if(r.getCpState() == 0) {
								state = "§rNeutral";
							}else if(r.getCpState() > 0) {
								double stateValue = (((double)r.getCpState())/16D) * 100D;
								state = "§a" + (int)stateValue + "% gut";
							}else if(r.getCpState() < 0) {
								double stateValue = ((((double)r.getCpState())*(-1D))/16D) * 100D;
								state = "§7" + (int)stateValue + "% böse";
							}
							p.sendMessage("§e- " + r.getName() + ": " + state);
						}
						p.sendMessage("§e===================================================");
					}else {
						p.sendMessage("§cBitte gib eine gültige Stadt an!");
					}
				}
			}else if(args[0].equalsIgnoreCase("set")) { // /city set owner DolGuldur ToppseDev
				if(Rank.get(p).isHigherThen(Rank.CMOD)) {
					if(args.length >= 3) {
						if(args[1].equalsIgnoreCase("owner") && args.length == 4) {
							City c = ArdaCraftAPI.getACDatabase().getCityManager().get(args[2]);
							if(c != null) {
								User u = User.getByName(args[3]);
								if(u != null) {
									c.setOwner(u);
									ArdaCraftAPI.getACDatabase().getUserManager().update(u);
									p.sendMessage("§aGründer erfolgreich geändert");
								}else {
									p.sendMessage("§cBitte gib einen gültigen Spieler an!");
								}
							}else {
								p.sendMessage("§cBitte gib eine Gültige Stadt an!");
							}
						}else if(args[1].equalsIgnoreCase("position") && args.length == 3) {
							City c = ArdaCraftAPI.getACDatabase().getCityManager().get(args[2]);
							if(c != null) {
								c.setLocation(p.getLocation());
								ArdaCraftAPI.getACDatabase().getCityManager().update(c);
								p.sendMessage("§aPosition erfolgreich geändert!");
							}else {
								p.sendMessage("§cBitte gib eine Gültige Stadt an!");
							}
						}else {
							p.sendMessage("§cBenutzung: /city set <owner|position> <Stadtname> [Player]");
						}
					}else {
						p.sendMessage("§cBenutzung: /city set <owner|position> <Stadtname> [Player]");
						
					}
				}else {
					p.sendMessage("§cDu hast keine Berechtigung für diesen Befehl!");
				}
			}else {
				showHelp(p);
				return true;
			}
		}

		return true;
	}
	
	public void showHelp(Player p) {
		p.sendMessage("§2Hilfe:");
		p.sendMessage("§e/city §blist §e- Listet alle in der Datenbank eingetragenen Städte auf");
		p.sendMessage("§e/city §bradar §e- Zeigt die entfernung zu allen Städten an");
		if(Rank.get(p).isHigherThen(Rank.SPIELER)) {
			p.sendMessage("§e/city §btp §e<Stadtname> - Teleportiert dich nach <Stadtname>");
		}
		if(Rank.get(p).isHigherThen(Rank.CMOD)) {
			p.sendMessage("§e/city §cdelete §e<Stadtname> - Löscht die Stadt <Stadtname> aus der Datanbank");
			p.sendMessage("§e/city §acreate §e<Stadtname> <Gründer> <good|bad> - Fügt die Stadt <Stadtname> der Datenbank hinzu");
			p.sendMessage("§e/city §dset §e<owner|position> <Stadtname> [Player] - Editiert die Stadt <Stadtname>");
		}
	}
	
}
