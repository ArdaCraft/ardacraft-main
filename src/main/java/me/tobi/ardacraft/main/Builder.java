package me.tobi.ardacraft.main;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class Builder {
	
	@SuppressWarnings("deprecation")
	public static Inventory getCategories(Player p) {
		Inventory inv = Bukkit.createInventory(p, 6*9, "Kategorien");
		
		ItemStack c1 = new ItemStack(53); //stairs
		ItemMeta c1meta = c1.getItemMeta();
		c1meta.setDisplayName("§2Treppen");
		c1.setItemMeta(c1meta);
		ItemStack c2 = new ItemStack(44); //slabs
		ItemMeta c2meta = c2.getItemMeta();
		c2meta.setDisplayName("§2Halbstufen");
		c2.setItemMeta(c2meta);
		ItemStack c3 = new ItemStack(155); //quartz
		ItemMeta c3meta = c3.getItemMeta();
		c3meta.setDisplayName("§2Quartz");
		c3.setItemMeta(c3meta);
		ItemStack c4 = new ItemStack(17); //wood
		ItemMeta c4meta = c4.getItemMeta();
		c4meta.setDisplayName("§2Holz");
		c4.setItemMeta(c4meta); 
		ItemStack c5 = new ItemStack(85);//fences
		ItemMeta c5meta = c5.getItemMeta();
		c5meta.setDisplayName("§2Zäune");
		c5.setItemMeta(c5meta);
		ItemStack c7 = new ItemStack(1); //stones
		ItemMeta c7meta = c7.getItemMeta();
		c7meta.setDisplayName("§2Steine");
		c7.setItemMeta(c7meta);
		ItemStack c8 = new ItemStack(12); //Sand
		ItemMeta c8meta = c8.getItemMeta();
		c8meta.setDisplayName("§2Sand");
		c8.setItemMeta(c8meta);
		ItemStack c9 = new ItemStack(20); //Glass
		ItemMeta c9meta = c9.getItemMeta();
		c9meta.setDisplayName("§2Glass");
		c9.setItemMeta(c9meta);
		ItemStack c10 = new ItemStack(2); //dirt
		ItemMeta c10meta = c10.getItemMeta();
		c10meta.setDisplayName("§2Erde");
		c10.setItemMeta(c10meta);
		ItemStack c11 = new ItemStack(331); //redstone
		ItemMeta c11meta = c11.getItemMeta();
		c11meta.setDisplayName("§2Redstone");
		c11.setItemMeta(c11meta);
		
		inv.addItem(c1);
		inv.addItem(c2);
		inv.addItem(c3);
		inv.addItem(c4);
		inv.addItem(c5);
		inv.addItem(c7);
		inv.addItem(c8);
		inv.addItem(c9);
		inv.addItem(c10);
		inv.addItem(c11);
		
		return inv;
	}
	
	@SuppressWarnings("deprecation")
	public static Inventory getInv(ItemStack clicked, Player p) {
		String name = clicked.getItemMeta().getDisplayName();
		if(name == null) {
			return null;
		}
		Inventory rtn = Bukkit.createInventory(p, 6*9, name);
		List<ItemStack> items = new ArrayList<ItemStack>();
		int id = clicked.getTypeId();
		if(id == 53) {
			items.add(new ItemStack(53));
			items.add(new ItemStack(67));
			items.add(new ItemStack(108));
			items.add(new ItemStack(109));
			items.add(new ItemStack(114));
			items.add(new ItemStack(128));
			items.add(new ItemStack(134));
			items.add(new ItemStack(135));
			items.add(new ItemStack(136));
			items.add(new ItemStack(156));
			items.add(new ItemStack(163));
			items.add(new ItemStack(164));
			items.add(new ItemStack(180));
		}
		else if(id == 44) {
			items.add(new ItemStack(44, 1, (short) 0, (byte) 0));
			items.add(new ItemStack(44, 1, (short) 0, (byte) 1));
			items.add(new ItemStack(44, 1, (short) 0, (byte) 3));
			items.add(new ItemStack(44, 1, (short) 0, (byte) 4));
			items.add(new ItemStack(44, 1, (short) 0, (byte) 5));
			items.add(new ItemStack(44, 1, (short) 0, (byte) 6));
			items.add(new ItemStack(44, 1, (short) 0, (byte) 7));
			items.add(new ItemStack(126, 1, (short) 0, (byte) 0));
			items.add(new ItemStack(126, 1, (short) 0, (byte) 1));
			items.add(new ItemStack(126, 1, (short) 0, (byte) 2));
			items.add(new ItemStack(126, 1, (short) 0, (byte) 3));
			items.add(new ItemStack(126, 1, (short) 0, (byte) 4));
			items.add(new ItemStack(126, 1, (short) 0, (byte) 5));
			items.add(new ItemStack(182));
		}
		else if(id == 155) {
			items.add(new ItemStack(155, 1, (short) 0, (byte) 0));
			items.add(new ItemStack(155, 1, (short) 0, (byte) 1));
			items.add(new ItemStack(155, 1, (short) 0, (byte) 2));
			items.add(new ItemStack(156));
		}
		else if(id == 17) {
			items.add(new ItemStack(17, 1, (short) 0, (byte) 0));
			items.add(new ItemStack(17, 1, (short) 0, (byte) 1));
			items.add(new ItemStack(17, 1, (short) 0, (byte) 2));
			items.add(new ItemStack(17, 1, (short) 0, (byte) 3));
			items.add(new ItemStack(162, 1, (short) 0, (byte) 0));
			items.add(new ItemStack(162, 1, (short) 0, (byte) 1));
		}
		else if(id == 85) {
			items.add(new ItemStack(85));
			items.add(new ItemStack(188));
			items.add(new ItemStack(189));
			items.add(new ItemStack(190));
			items.add(new ItemStack(191));
			items.add(new ItemStack(192));
			items.add(new ItemStack(139, 1, (short) 0, (byte) 0));
			items.add(new ItemStack(139, 1, (short) 0, (byte) 1));
			items.add(new ItemStack(107));
			items.add(new ItemStack(183));
			items.add(new ItemStack(184));
			items.add(new ItemStack(185));
			items.add(new ItemStack(186));
			items.add(new ItemStack(187));
		}
		else if(id == 1) {
			items.add(new ItemStack(1, 1, (short) 0, (byte) 0));
			items.add(new ItemStack(1, 1, (short) 0, (byte) 1));
			items.add(new ItemStack(1, 1, (short) 0, (byte) 3));
			items.add(new ItemStack(1, 1, (short) 0, (byte) 4));
			items.add(new ItemStack(1, 1, (short) 0, (byte) 5));
			items.add(new ItemStack(1, 1, (short) 0, (byte) 6));
			items.add(new ItemStack(4));
			items.add(new ItemStack(45));
			items.add(new ItemStack(45));
			items.add(new ItemStack(98, 1, (short) 0, (byte) 0));
			items.add(new ItemStack(98, 1, (short) 0, (byte) 1));
			items.add(new ItemStack(98, 1, (short) 0, (byte) 2));
			items.add(new ItemStack(98, 1, (short) 0, (byte) 3));
		}
		
		for(ItemStack i : items) {
			rtn.addItem(i);
		}
		return rtn;
	}

	public static boolean handleInventoryClick(Inventory inv, ItemStack item, Player p) {
		if(inv.getName().equalsIgnoreCase("Kategorien")) {
			p.openInventory(Builder.getInv(item, p));
			return true;
		}else if(inv.getName().contains("§2")) {
			item.setAmount(item.getMaxStackSize());
			p.getInventory().addItem(item);
			item.setAmount(1);
			return true;
		}
		return false;
	}

}
